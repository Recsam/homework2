@component('components.formmodal')
    @slot('id', 'reply-modal')
    @slot('title', 'Emails')
    @slot('body')
        <div class="form-group">
            <label for="inputTo">To</label>
            <input type="text" class="form-control" id="inputTo">
        </div>
        <div class="form-group">
            <label for="inputSubject">Subject</label>
            <input type="text" class="form-control" id="inputSubject">
        </div>
        <div class="form-group">
            <label for="inputMessage">Message</label>
            <textarea style="min-height: 10rem" class="form-control" id="inputMessage"></textarea>
        </div>
    @endslot
    @slot('actions')
        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-outline-secondary">Send</button>
    @endslot
@endcomponent
