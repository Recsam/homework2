@extends('layouts.app')

@section('title', 'Contact')

@section('pageTitle', 'Contact List')

@section('body')
    <div class="d-flex justify-content-end mb-3">
        <button id="btnBulkEmails" class="btn btn-outline-secondary text-red text-uppercase">Bulk Emails</button>
    </div>
    <form class="d-flex justify-content-between mb-2" style="overflow-x: scroll;">
        <div class="d-flex">
            <select class="form-control" style="width: 120px">
                <option>Bulk Actions</option>
            </select>
            <button class="btn btn-outline-secondary text-uppercase ml-2">Apply</button>
            <select class="form-control ml-2" style="width: 120px">
                <option>Status</option>
            </select>
            <button class="btn btn-outline-secondary text-uppercase ml-2">Filter</button>
        </div>
        <div class="d-flex">
            <input type="text" class="form-control ml-2" placeholder="Entry Keyword ..." style="min-width: 200px">
            <button class="btn btn-outline-secondary text-uppercase ml-2">Search</button>
        </div>
    </form>
    @include('lists.contacts')
    <nav class="d-flex justify-content-between">
        <p>Total items: <b>50</b></p>
        <ul class="pagination">
            <li class="page-item">
                <a class="page-link" href="#" aria-label="Previous">
                    <span aria-hidden="true"><i class="fa fa-chevron-left"></i></span>
                    <span class="sr-only">Previous</span>
                </a>
            </li>
            <li class="page-item"><a class="page-link" href="{{ route('admin.contacts.index') . '?page=' . 1 }}">1</a></li>
            <li class="page-item"><a class="page-link" href="{{ route('admin.contacts.index') . '?page=' . 2 }}">2</a></li>
            <li class="page-item"><a class="page-link" href="{{ route('admin.contacts.index') . '?page=' . 3 }}">3</a></li>
            <li class="page-item"><a class="page-link" href="{{ route('admin.contacts.index') . '?page=' . 4 }}">4</a></li>
            <li class="page-item"><a class="page-link" href="{{ route('admin.contacts.index') . '?page=' . 5 }}">5</a></li>
            <li class="page-item">
                <a class="page-link" href="#" aria-label="Next">
                    <span aria-hidden="true"><i class="fa fa-chevron-right"></i></span>
                    <span class="sr-only">Next</span>
                </a>
            </li>
        </ul>
    </nav>
    @include('modals.bulkemails')
    @push('script')
        <script>
            $(function () {
                $('#btnBulkEmails').on('click', function (event) {
                    $('#bulk-email-modal').modal('show');
                })
            })
        </script>
    @endpush
@stop

