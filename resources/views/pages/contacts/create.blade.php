@extends('layouts.app')

@section('title', 'Contact')

@section('pageTitle', 'Contact')

@section('body')
    <form>
        <div class="form-group">
            <label for="inputName">Name <span class="text-danger">*</span></label>
            <input type="text" class="form-control" id="inputName" required>
        </div>
        <div class="row">
            <div class="form-group col-6">
                <label for="inputEmail">Email <span class="text-danger">*</span></label>
                <input type="password" class="form-control" id="inputEmail" required>
            </div>
            <div class="form-group col-6">
                <label for="inputPhoneNumber">Phone Number</label>
                <input type="tel" class="form-control" id="inputPhoneNumber">
            </div>
        </div>
        <div class="form-group">
            <label for="inputSubject">Subject</label>
            <input type="text" class="form-control" id="inputSubject">
        </div>
        <div class="form-group">
            <label for="inputMessage">Message</label>
            <textarea style="min-height: 10rem" class="form-control" id="inputMessage"></textarea>
        </div>
        <div class="d-flex justify-content-end">
            <a href="{{ route('contacts.confirm') }}" class="btn btn-outline-secondary">Next <i class="fa fa-chevron-right"></i></a>
        </div>
    </form>
@stop
