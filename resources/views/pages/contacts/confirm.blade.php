@extends('layouts.app')

@section('title', 'Contact')

@section('pageTitle', 'Contact Confirmation')

@section('body')
    <div>
        @include('lists.viewcontact')
        <div class="d-flex justify-content-between">
            <a href="{{ route('contacts.create') }}" class="btn btn-outline-secondary"><i class="fa fa-chevron-left"></i> Back</a>
            <a href="{{ route('contacts.success') }}" class="btn btn-outline-secondary">Submit</a>
        </div>
    </div>
@stop
