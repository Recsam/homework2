<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ContactController extends Controller
{

    public function create() {
        return view('pages.contacts.create');
    }

    public function confirm() {
        $contact_information = (object) [
            'name' => 'Mr, yimrak sop hear',
            'email' => 'yimraksophear@dev.com',
            'phone_number' => '012334455',
            'subject_name' => 'Support',
            'message' => 'Consectetuer, penatibus. Sagittis sequi quidem sit hic mauris? Officia deleniti repellat fugiat
sem voluptatibus! Nihil. Adipisicing! Aptent interdum eaque veniam reprehenderit ab aliqua
dictumst, ornare quis, doloremque, commodi irure lorem laborum suscipit, mattis feugiat
accusantium etiam dignissimos reiciendis officia autem ullamco deleniti eaque sunt irure
accumsan rutrum lorem parturient phasellus, commodi magni. Dolore unde ipsum est quis, ac,<br><br>
dictumst elementum, eaque est dictumst cum! Nulla. Minim tempore bibendum laudantium
placeat nemo dignissimos, inventore quaerat officia quisquam auctor nisl? Aliqua facilisis.
Pharetra, ornare expedita error. Esse cras libero eget velit ullam, tristique eveniet diamlorem
nullam quos! Nemo curae minim? Nemo class. Consectetuer, penatibus. Sagittis sequi quidem<br><br>
sit hic mauris? Officia deleniti repellat fugiat
sem voluptatibus! Nihil. Adipisicing! Aptent interdum eaque veniam reprehenderit ab aliqua
dictumst, ornare quis, doloremque, commodi irure lorem'
        ];
        return view('pages.contacts.confirm')->with('contact_information', $contact_information);
    }

    public function success() {
        return view('pages.contacts.success');
    }

}
